import axios from "axios";

export default async function addProduct(product) {
  const cartData = JSON.parse(localStorage.getItem("cart"));
  if (cartData) {
    const res = await axios.post("https://dummyjson.com/carts/add", {
      userId: 5,
      products: [
        ...cartData.products,
        {
          id: product.id,
          quantity: 1,
        },
      ],
    });
    return res.data;
  } else {
    const res = await axios.post("https://dummyjson.com/carts/add", {
      userId: 5,
      products: [
        {
          id: product.id,
          quantity: 1,
        },
      ],
    });
    return res.data;
  }
}
