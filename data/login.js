import axios from "axios";

export default async function login(loginData) {
  const res = await axios.post("https://dummyjson.com/auth/login", loginData);
  return res;
}
