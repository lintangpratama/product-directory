import axios from "axios";

export default async function getProductCategories() {
  try {
    const res = await axios.get("https://dummyjson.com/products/categories");
    return res.data;
  } catch (e) {
    console.log(e);
  }
}
