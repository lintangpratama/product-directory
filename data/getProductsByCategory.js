import axios from "axios";

export default async function getProductsByCategory(category) {
  try {
    const res = await axios.get(
      `https://dummyjson.com/products/category/${category}`
    );
    return res.data.products;
  } catch (e) {
    console.log(e);
  }
}
