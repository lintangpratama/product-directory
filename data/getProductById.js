import axios from "axios";

export default async function getProductById(id) {
  try {
    const res = await axios.get(`https://dummyjson.com/products/${id}`);
    return res.data;
  } catch (e) {
    console.log(e);
  }
}
