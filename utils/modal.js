import Swal from "sweetalert2";

export default function modal(icon, title, text) {
  Swal.fire({
    icon: icon,
    title: title,
    text: text,
  });
}
