const countTotalPrice = (arr) => {
    let total = 0;
    arr.forEach(product => total += product.price * product.quantity);
    return total;
}

export default countTotalPrice;