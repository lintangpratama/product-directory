import { NextResponse } from "next/server";

// This function can be marked `async` if using `await` inside
export function middleware(request) {
  const cookie = request.cookies.get("woishop_token");
  if (!cookie) {
    return NextResponse.redirect(new URL("/auth/login", request.url));
  }
}

// See "Matching Paths" below to learn more
export const config = {
  matcher: ["/cart/:path*", "/category/:path*",  "/product/:path*", "/"],
};
