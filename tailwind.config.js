/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        main: "#03AC0E",
        "sub-main": "#7A7A7A",
        "background-main": "#F1F1F1",
        black: "#292929",
        success: "#4E9D64",
        "white-500": "#EBEBEB",
        "background-input": "#F5F5F5",
        danger: "#F62845",
        grey: "#C8C8C8",
        "bg-card": "#FAFAFA",
        "green": "#31B057",
        "green-600": "#049C0E"
      },
      maxWidth: {
        "mobile-screen": "416px",
      },
      maxHeight: {
        input: "52px",
      },
      margin: {
        50: "50px",
      },
      fontSize: {
        xxs: "10px",
        small: "12px"
      },
      width: {
        '26': '108px',
      }
    },
  },
  plugins: [],
};
