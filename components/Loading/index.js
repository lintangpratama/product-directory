export default function Loading() {
  return (
    <div>
      <div>
        <img
          src="/animated-icon/loading.svg"
          className="mx-auto text-xs"
          alt="Loading"
        />
      </div>
    </div>
  );
}
