import Image from "next/image";
import { useState } from "react";

export default function DeliveryOption() {
  const [deliveryOptions, setDeliveryOptions] = useState([
    {
      img: "/cart/icon-pelayanan.svg",
      imgTwo: null,
      name: "Pickup / diambil",
      isActive: true,
    },
    {
      img: "/cart/logo.svg",
      imgTwo: null,
      name: "Ekspress / Kirim Cepat",
      isActive: false,
    },
    {
      img: "/cart/logo.svg",
      imgTwo: "/cart/clock.svg",
      name: "09.00 - 11.00",
      isActive: false,
    },
    {
      img: "/cart/logo.svg",
      imgTwo: "/cart/clock.svg",
      name: "12.00 - 16.00",
      isActive: false,
    },
    {
      img: "/cart/logo.svg",
      imgTwo: "/cart/clock.svg",
      name: "18.00 - 20.00",
      isActive: false,
    },
  ]);
  return (
    <>
      {deliveryOptions.map((option, i) => (
        <div
          key={i}
          id={`delivery${i}`}
          onClick={(e) => {
            setDeliveryOptions([
              ...deliveryOptions.map((option, index) => {
                if (index === i) {
                  return {
                    img: option.img,
                    imgTwo: option.imgTwo,
                    name: option.name,
                    isActive: true,
                  };
                } else {
                  return {
                    img: option.img,
                    imgTwo: option.imgTwo,
                    name: option.name,
                    isActive: false,
                  };
                }
              }),
            ]);
          }}
          className="flex p-4 cursor-pointer items-center mt-4 justify-between bg-bg-card border border-white-500 rounded-2xl"
        >
          <div className="flex items-center">
            <Image src={option.img} width={32} height={32} alt="alt" />
            {option.imgTwo ? (
              <img
                src="/cart/clock.svg"
                width={12}
                height={12}
                alt="pelayanan"
                className="-ml-3.5 mt-2.5 z-10"
              />
            ) : null}
            <h2 className="text-black text-sm font-semibold ml-4">
              {option.name}
            </h2>
          </div>
          {option.isActive ? (
            <div className="flex items-center justify-center bg-white w-6 h-6 rounded-full border-2 border-green">
              <div className="mx-auto my-auto bg-green z-10 w-4 h-4 rounded-full"></div>
            </div>
          ) : (
            <div className="flex items-center justify-center bg-white w-6 h-6 rounded-full border-2 border-grey">
              <div className="mx-auto my-auto bg-grey z-10 w-4 h-4 rounded-full "></div>
            </div>
          )}
        </div>
      ))}
    </>
  );
}
