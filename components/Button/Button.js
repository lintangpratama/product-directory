export default function Button(props) {
  const { placeholder } = props;
  return (
    <>
        <button className="bg-main rounded-full mt-5 w-full">
          <h2 className="font-semibold text-sm py-4 text-white">
            {placeholder}
          </h2>
        </button>
    </>
  );
}
