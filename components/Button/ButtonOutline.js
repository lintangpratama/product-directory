import Image from "next/image";

export default function ButtonOutline(props) {
  const { placeholder, imgSrc } = props;
  return (
    <>
      <button className="flex justify-center items-center bg-white border border-main rounded-full mt-5 w-full">
        <Image src={imgSrc} width={16} height={16} alt="img-button" />
        <h2 className="font-semibold text-sm py-4 text-black ml-1.5">{placeholder}</h2>
      </button>
    </>
  );
}
