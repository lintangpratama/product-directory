import Cookies from "js-cookie";
import { useRouter } from "next/router";

export default function LogoutButton() {
  const router = useRouter();
  return (
    <button
      onClick={() => {
        Cookies.remove("woishop_token");
        router.push("/auth/login");
      }}
      className="bg-red-600 rounded-full mt-10 w-full"
    >
      <h2 className="font-semibold text-sm py-4 text-white">Keluar</h2>
    </button>
  );
}
