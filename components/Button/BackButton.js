import Image from "next/image";
import { useRouter } from "next/router";

export default function BackButton() {
  const router = useRouter();
  return (
    <button onClick={() => router.back()} className="flex justify-center">
      <Image
        src="/icon/icon-arrow.svg"
        width={20}
        height={20}
        alt="arrow-icon"
      />
    </button>
  );
}
