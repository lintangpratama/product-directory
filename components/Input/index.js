import Image from "next/image";

export default function Input({
  img,
  isValid,
  isFocused,
  label,
  errorMsg,
  name,
  id,
  type,
  placeholder,
  defaultValue,
  onChange,
  onFocus,
  onBlur,
}) {
  return (
    <>
      <div
        className={
          !isValid
            ? "flex w-full py-4 px-6 rounded-full bg-background-input px-auto justify-center border border-danger mt-4 max-h-input"
            : "flex w-full py-4 px-6 rounded-full bg-background-input px-auto justify-center mt-4 max-h-input"
        }
      >
        <Image
          src={img}
          alt="icon-account"
          width={16}
          height={16}
        />
        <div
          className={
            isFocused
              ? "w-full flex flex-col ml-3 -mt-1.5 transition-all"
              : "w-full flex flex-col ml-3"
          }
        >
          <label
            htmlFor="username"
            className={
              isFocused
                ? "font-semibold text-xxs text-sub-main -mb-0.5"
                : "font-semibold text-xxs text-sub-main -mb-0.5 hidden"
            }
          >
            {label}
          </label>
          <input
            id={id}
            name={name}
            type={type}
            placeholder={placeholder}
            defaultValue={defaultValue}
            className="bg-background-input text-black text-sm w-full placeholder:text-sm placeholder:text-sub-main"
            onChange={onChange}
            onFocus={onFocus}
            onBlur={onBlur}
          />
        </div>
      </div>
      {!isValid ? (
        <div className="mt-2">
          <p className="text-center text-xxs text-danger">
            {errorMsg}
          </p>
        </div>
      ) : null}
    </>
  );
}
