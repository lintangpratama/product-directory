export default function ProductDescription({ title, price, description }) {
  return (
    <>
      <h2 className="font-semibold text-sm mt-4">{title}</h2>
      <h2 className="font-semibold text-sm mt-1 mb-6">{`$${price}`}</h2>
      <hr />
      <h2 className="font-semibold text-sm mt-6">Deskripsi Produk</h2>
      <p className="font-normal text-xs text-sub-main mt-1.5">{description}</p>
    </>
  );
}
