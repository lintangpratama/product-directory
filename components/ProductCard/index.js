import Link from "next/link";
import Image from "next/image";
import Skeleton from "react-loading-skeleton";
import "react-loading-skeleton/dist/skeleton.css";

export default function ProductCard({
  id,
  thumbnail,
  title,
  price,
  isLoading,
  onLoad,
  onClick,
}) {
  return (
    <>
      <Link key={id} href={`/product/${id}`}>
        <div className="cursor-pointer hover:underline">
          {isLoading ? <Skeleton className="w-full" height={104} /> : null}
          <div className="rounded-lg border border-white-500">
            <Image
              src={thumbnail}
              alt="category"
              width={104}
              height={104}
              objectFit="contain"
              onLoad={onLoad}
            />
          </div>
          <p className="font-normal text-left text-xs text-black mt-4 capitalize h-8">
            {title}
          </p>
        </div>
      </Link>
      <h2 className="font-semibold text-sm mt-4">{`$${price}`}</h2>
    </>
  );
}
