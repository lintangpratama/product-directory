export default function MainContainer({ children }) {
  return <div className="mx-4 my-3">{children}</div>;
}
