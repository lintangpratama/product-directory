export default function MobileContainer({ children }) {
  return (
    <div className="flex flex-col">
      <div className="h-full bg-gray-100">
        <div className="block box-border bg-white max-w-md w-full mx-auto h-full">
          {children}
        </div>
      </div>
    </div>
  );
}
