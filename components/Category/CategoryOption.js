import Link from "next/link";
import Image from "next/image";

export default function CategoryOption({ id, category }) {
  return (
    <Link key={id} href={`/category/${category}`}>
      <div className="flex flex-col text-center cursor-pointer hover:underline">
        <Image
          src="/category/category.svg"
          alt="category"
          width={73}
          height={73}
        />
        <p className="font-normal text-xs text-sub-main mt-4 capitalize">
          {category}
        </p>
      </div>
    </Link>
  );
}
