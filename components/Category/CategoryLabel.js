export default function CategoryLabel({ category }) {
  return (
    <>
      <div className="bg-white-500 rounded-full w-fit">
        <p className="text-xs font-semibold px-3 py-0.5 text-sub-main mt-4 capitalize">
          {category}
        </p>
      </div>
    </>
  );
}
