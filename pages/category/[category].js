/* eslint-disable @next/next/no-img-element */
import { useEffect, useState } from "react";
import { useRouter } from "next/router";

import BackButton from "components/Button/BackButton";
import Loading from "components/Loading";
import MainContainer from "components/Container/MainContainer";
import ProductCard from "components/ProductCard";
import getProductsByCategory from "data/getProductsByCategory";
import addProduct from "data/addProduct";

export default function ProductCategoryDetail() {
  const [productList, setProductList] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [imageIsLoading, setImageIsLoading] = useState(true);

  const router = useRouter();
  const { category } = router.query;

  useEffect(() => {
    if (category) {
      setIsLoading(true);
      getProductsByCategory(category).then((products) => {
        setProductList(products);
        setIsLoading(false);
      });
    }
  }, []);

  const addProductHandler = async (product) => {
    addProduct(product).then((data) => {
      localStorage.setItem("cart", JSON.stringify(data));
      router.push("/cart");
    });
  };

  return (
    <>
      <div className="flex flex-col min-h-screen max-h-full bg-white">
        <div className="shadow-sm">
          <div className="flex items-center py-4 mx-4">
            <BackButton />
            <h1 className="text-black text-base font-semibold capitalize ml-4">
              {category}
            </h1>
          </div>
        </div>
        {isLoading ? (
          <div className="mx-4 my-4 min-h-screen flex flex-col justify-center">
            <Loading />
          </div>
        ) : (
          <MainContainer>
            <div className="grid grid-cols-3 justify-between gap-5">
              {productList.map((product) => (
                <>
                  <div className="flex flex-col text-center mb-8">
                    <ProductCard
                      id={product.id}
                      title={product.title}
                      thumbnail={product.thumbnail}
                      price={product.price}
                      isLoading={imageIsLoading}
                      onLoad={() => setImageIsLoading(false)}
                    />
                    <button
                      onClick={() => {
                        addProductHandler(product);
                      }}
                      className="border border-main py-2 rounded-full text-center w-full mt-4"
                    >
                      <h2 className="font-semibold text-sm text-main">
                        Tambah
                      </h2>
                    </button>
                  </div>
                </>
              ))}
            </div>
          </MainContainer>
        )}
      </div>
    </>
  );
}
