/* eslint-disable @next/next/no-img-element */
import Image from "next/image";
import { useState, useEffect } from "react";

import Input from "components/Input";
import Loading from "components/Loading";
import MainContainer from "components/Container/MainContainer";
import BackButton from "components/Button/BackButton";

export default function Register() {
  const [isLoading, setIsLoading] = useState(true);
  const [isFocused, setIsFocused] = useState({
    name: false,
    phone: false,
    referral: false,
  });
  const [isValid, setIsValid] = useState({
    name: true,
    phone: true,
  });
  const [loginData, setLoginData] = useState({
    name: "",
    phone: "",
    referral: "",
  });

  const checkNameInputValidation = () => {
    const name = document.getElementById("name").value;

    name.length > 0
      ? setIsValid({
          ...isValid,
          name: true,
        })
      : setIsValid({
          ...isValid,
          name: false,
        });
  };

  const checkPhoneInputValidation = () => {
    const phone = document.getElementById("phone").value;

    phone.length > 0
      ? setIsValid({
          ...isValid,
          phone: true,
        })
      : setIsValid({
          ...isValid,
          phone: false,
        });
  };

  const onChangeHandler = (e) => {
    const inputName = e.target.getAttribute("name");
    setLoginData({
      ...loginData,
      [inputName]: e.target.value,
    });
  };

  useEffect(() => {
    setIsLoading(false);
  }, []);

  return (
    <>
      <div className="flex flex-col min-h-screen max-h-full bg-white">
        <div className="shadow-sm">
          <div className="flex items-center py-4 mx-4">
            <BackButton />
            <h1 className="text-black text-base font-semibold capitalize ml-4">
              Daftar Baru
            </h1>
          </div>
        </div>
        {isLoading ? (
          <div className="mx-4 my-4 min-h-screen flex flex-col justify-center">
            <Loading />
          </div>
        ) : (
          <MainContainer>
            <div className="flex flex-col">
              <form>
                <Input
                  name="name"
                  id="name"
                  type="text"
                  label="Nama Lengkap"
                  img="/icon/icon-account.svg"
                  placeholder="Nama Lengkap"
                  defaultValue=""
                  errorMsg="Nama Tidak Boleh Kosong"
                  isValid={isValid.name}
                  isFocused={isFocused.name}
                  onChange={(e) => {
                    checkNameInputValidation();
                    onChangeHandler(e);
                  }}
                  onFocus={(e) => {
                    setIsFocused({
                      ...isFocused,
                      name: true,
                    });
                  }}
                  onBlur={(e) => {
                    setIsFocused({
                      ...isFocused,
                      name: false,
                    });
                  }}
                />

                <Input
                  name="phone"
                  id="phone"
                  type="text"
                  label="Nomor HP"
                  img="/icon/icon-phone.svg"
                  placeholder="Nomor HP"
                  defaultValue=""
                  errorMsg="Nomor Tidak Boleh Kosong"
                  isValid={isValid.phone}
                  isFocused={isFocused.phone}
                  onChange={(e) => {
                    checkPhoneInputValidation();
                    onChangeHandler(e);
                  }}
                  onFocus={(e) => {
                    setIsFocused({
                      ...isFocused,
                      phone: true,
                    });
                  }}
                  onBlur={(e) => {
                    setIsFocused({
                      ...isFocused,
                      phone: false,
                    });
                  }}
                />
                
                <Input
                  name="referral"
                  id="referral"
                  type="text"
                  label="Kode Referal (Opsional)"
                  img="/icon/icon-referal.svg"
                  placeholder="Kode Referal (Opsional)"
                  defaultValue=""
                  errorMsg=""
                  isValid={true}
                  isFocused={isFocused.referral}
                  onChange={(e) => {
                    onChangeHandler(e);
                  }}
                  onFocus={(e) => {
                    setIsFocused({
                      ...isFocused,
                      referral: true,
                    });
                  }}
                  onBlur={(e) => {
                    setIsFocused({
                      ...isFocused,
                      referral: false,
                    });
                  }}
                />

                <button className="bg-main rounded-full mt-5 w-full">
                  <h2 className="font-semibold text-sm py-4 text-white">
                    Daftar
                  </h2>
                </button>
              </form>
            </div>
          </MainContainer>
        )}
      </div>
    </>
  );
}
