/* eslint-disable @next/next/no-img-element */
import ButtonOutline from "components/Button/ButtonOutline";

import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";

import { useEffect, useState } from "react";
import Cookies from "js-cookie";
import Loading from "components/Loading";
import modal from "utils/modal";
import login from "data/login";
import Input from "components/Input";

export default function Login() {
  const [isLoading, setIsLoading] = useState(true);
  const [isFocused, setIsFocused] = useState({
    username: true,
    password: true,
  });
  const [isValid, setIsValid] = useState({
    username: true,
    password: true,
  });
  const [loginData, setLoginData] = useState({
    username: "kminchelle",
    password: "0lelplR",
  });
  const router = useRouter();

  const checkUsernameInputValidation = () => {
    const username = document.getElementById("username").value;

    username.length > 0
      ? setIsValid({
          ...isValid,
          username: true,
        })
      : setIsValid({
          ...isValid,
          username: false,
        });
  };

  const checkPasswordInputValidation = () => {
    const password = document.getElementById("password").value;

    password.length > 0
      ? setIsValid({
          ...isValid,
          password: true,
        })
      : setIsValid({
          ...isValid,
          password: false,
        });
  };

  const onChangeHandler = (e) => {
    const inputName = e.target.getAttribute("name");
    setLoginData({
      ...loginData,
      [inputName]: e.target.value,
    });
  };

  const onSubmitHandler = async () => {
    setIsLoading(true);
    login(loginData)
      .then((res) => {
        setIsLoading(false);
        if (res.status === 200) {
          Cookies.set("woishop_token", res.data.token);
          router.push("/");
        } else {
          modal("error", "Login Failed", "Invalid Credential!");
        }
      })
      .catch(() => {
        setIsLoading(false);
        modal("error", "Login Failed", "Invalid Credential!");
      });
  };

  useEffect(() => {
    if (Cookies.get("woishop_token")) {
      router.push("/");
    }
    setIsLoading(false);
  }, []);

  return (
    <>
      <div className="flex flex-col min-h-screen max-h-full bg-white">
        {isLoading ? (
          <div className="mx-4 my-4 min-h-screen flex flex-col justify-center">
            <Loading />
          </div>
        ) : (
          <div className="mx-4 my-4 bg-white">
            <div className="flex flex-col justify-center mt-50">
              <Image
                src="/img/auth/cuate.svg"
                alt="cuate-img"
                width={226}
                height={230}
              />
            </div>
            <div>
              <h1 className="text-main text-center font-bold text-2xl mt-8">
                Hai, fren!
              </h1>
              <h2 className="text-base text-center text-black">
                Selamat datang di aplikasi WoiShop
              </h2>
              <form onSubmit={onSubmitHandler}>
                {/* Usename input */}
                <Input
                  name="username"
                  id="username"
                  type="text"
                  label="Username"
                  img="/icon/icon-account.svg"
                  placeholder="Username Anda"
                  defaultValue="kminchelle"
                  errorMsg="Username Tidak Boleh Kosong"
                  isValid={isValid.username}
                  isFocused={isFocused.username}
                  onChange={(e) => {
                    checkUsernameInputValidation();
                    onChangeHandler(e);
                  }}
                  onFocus={(e) => {
                    setIsFocused({
                      ...isFocused,
                      username: true,
                    });
                  }}
                  onBlur={(e) => {
                    setIsFocused({
                      ...isFocused,
                      username: false,
                    });
                  }}
                />

                {/* Password input */}
                <Input
                  name="password"
                  id="password"
                  type="password"
                  label="Password"
                  img="/icon/icon-password.svg"
                  placeholder="Password Anda"
                  defaultValue="0lelplR"
                  errorMsg="Password Tidak Boleh Kosong"
                  isValid={isValid.password}
                  isFocused={isFocused.password}
                  onChange={(e) => {
                    checkPasswordInputValidation();
                    onChangeHandler(e);
                  }}
                  onFocus={(e) => {
                    setIsFocused({
                      ...isFocused,
                      password: true,
                    });
                  }}
                  onBlur={(e) => {
                    setIsFocused({
                      ...isFocused,
                      password: false,
                    });
                  }}
                />

                <button
                  onClick={(e) => {
                    onSubmitHandler(e);
                  }}
                  className="bg-main rounded-full mt-5 w-full"
                >
                  <h2 className="font-semibold text-sm py-4 text-white">
                    Masuk
                  </h2>
                </button>
              </form>
            </div>

            <div className="flex justify-center items-center mt-5">
              <div className="bg-white-500 h-0.5 w-1/3"></div>
              <p className="text-xs font-semibold text-sub-main text-center mx-2 w-26">
                Atau Login Melalui
              </p>
              <div className="bg-white-500 h-0.5 w-1/3"></div>
            </div>

            <ButtonOutline
              placeholder="Google"
              imgSrc="/icon/google-icon.svg"
            />

            <div className="text-center mt-4">
              <p className="text-xs text-sub-main">
                Belum punya akun?{" "}
                <Link href="/auth/register" passHref={true}>
                  <span className="hover:underline cursor-pointer">
                    Daftar di sini
                  </span>
                </Link>
              </p>
            </div>

            <div className="text-justify mt-6">
              <p className="text-xs text-sub-main">
                Dengan masuk atau mendaftar, kamu menyetujui Ketentuan Layanan
                dan Kebijakan Privasi
              </p>
            </div>
          </div>
        )}
      </div>
    </>
  );
}
