/* eslint-disable @next/next/no-img-element */
import Image from "next/image";
import Link from "next/link";
import { useState, useEffect } from "react";
import countTotalPrice from "utils/countTotalPrice";
import Loading from "components/Loading";
import DeliveryOption from "components/DeliveryOption";
import BackButton from "components/Button/BackButton";

export default function ProductCategory() {
  const [cartData, setCartData] = useState({
    products: [],
  });
  const [isLoading, setIsLoading] = useState(false);

  const deleteCartProductHandler = (productId) => {
    const data = cartData.products.filter(
      (cartProduct) => cartProduct.id !== productId
    );
    localStorage.setItem(
      "cart",
      JSON.stringify({
        ...cartData,
        products: data,
      })
    );
    setCartData({ ...cartData, products: data });
  };

  const updateCartProductHandler = (productId, command) => {
    const data = cartData.products.map((cartProduct) => {
      if (cartProduct.id === productId) {
        return {
          ...cartProduct,
          quantity:
            command === "add"
              ? cartProduct.quantity + 1
              : cartProduct.quantity - 1,
        };
      }
      return cartProduct;
    });
    const filteredData = data.filter((product) => product.quantity > 0);
    localStorage.setItem(
      "cart",
      JSON.stringify({
        ...cartData,
        products: filteredData,
      })
    );
    setCartData({ ...cartData, products: filteredData });
  };

  useEffect(() => {
    setIsLoading(true);
    const data = JSON.parse(localStorage.getItem("cart"));
    console.log(data);
    {
      data ? setCartData(data) : setCartData(cartData);
    }
    setIsLoading(false);
  }, []);

  return (
    <>
      <div className="flex flex-col min-h-screen max-h-full bg-white">
        {isLoading ? (
          <div className="mx-4 my-4 min-h-screen flex flex-col justify-center">
            <Loading />
          </div>
        ) : (
          <>
            <div className="min-h-screen">
              <div className="shadow-sm">
                <div className="flex items-center py-4 mx-4">
                  <BackButton />
                  <h1 className="text-black text-base font-semibold capitalize ml-4">
                    Keterangan
                  </h1>
                </div>
              </div>
              <div className="mx-4 my-4">
                <h1 className="text-base font-semibold text-black mt-6 shadow-sm pb-3">
                  Pilih Metode Pengiriman
                </h1>

                <DeliveryOption />

                <div className="flex mt-8 shadow-sm pb-3 justify-between items-center">
                  <h1 className="text-base font-semibold text-black">
                    Pesanan
                  </h1>
                  <Link href="/" passHref={true}>
                    <p className="text-small font-semibold text-green-600 cursor-pointer hover:underline">
                      + Tambah Pesanan
                    </p>
                  </Link>
                </div>

                {/* List products */}
                {cartData.products.map((product) => (
                  <div
                    key={product.id}
                    id={product.id}
                    className="flex mt-4 justify-between pb-6 border-b border-dashed"
                  >
                    <div className="flex">
                      <button
                        onClick={() => {
                          deleteCartProductHandler(product.id);
                        }}
                        className="self-center mr-4"
                      >
                        <Image
                          src="/cart/icon-cancel.svg"
                          width={24}
                          height={24}
                          alt="cancel"
                        />
                      </button>
                      <div>
                        <p className="text-sm font-normal mb-2 capitalize">
                          {product.title}
                        </p>
                        <h1 className="font-semibold text-base">{`$${product.price}`}</h1>
                      </div>
                    </div>
                    <div className="flex self-end">
                      <button
                        onClick={() => {
                          updateCartProductHandler(product.id, "min");
                        }}
                      >
                        <Image
                          src="/cart/icon-min.svg"
                          width={24}
                          height={24}
                          alt="min"
                        />
                      </button>
                      <h1 className="text-base font-medium mx-6">
                        {product.quantity}
                      </h1>
                      <button
                        onClick={() => {
                          updateCartProductHandler(product.id, "add");
                        }}
                      >
                        <Image
                          src="/cart/icon-plus.svg"
                          width={24}
                          height={24}
                          alt="plus"
                        />
                      </button>
                    </div>
                  </div>
                ))}

                {cartData.products.length < 1 ? (
                  <div className="flex justify-center">
                    <p className="text-sm font-normal mt-4 capitalize">
                      Belum Ada Pesanan
                    </p>
                  </div>
                ) : null}

                {cartData.products.length < 1 ? null : (
                  <div className="flex justify-between mt-6">
                    <h1 className="font-semibold text-sm">Total Harga</h1>
                    <h1 className="font-semibold text-sm">{`$${countTotalPrice(
                      cartData.products
                    )}`}</h1>
                  </div>
                )}
              </div>

              {cartData.products.length < 1 ? null : (
                <>
                  <div className="w-full h-[8px] bg-bg-card"></div>
                  <div className="mx-4 my-4">
                    <h1 className="text-black text-base font-semibold pb-4 border-b">
                      Tambahkan Catatan
                    </h1>
                    <div className="flex w-full py-4 px-6 rounded-2xl bg-bg-card px-auto justify-center mt-4 max-h-input">
                      <Image
                        src="/cart/icon-edit.svg"
                        alt="icon-phone"
                        width={16}
                        height={16}
                      />
                      <div className="w-full flex flex-col ml-3">
                        <input
                          name="name"
                          type="text"
                          placeholder="Masukkan catatan di sini"
                          className="bg-bg-card text-black text-sm w-full placeholder:text-sm placeholder:text-sub-main"
                        />
                      </div>
                    </div>
                  </div>
                  <div className="w-full h-1 bg-bg-card"></div>
                  <div className="mx-4 my-4">
                    <div className="flex justify-between">
                      <h1 className="font-semibold text-sm">
                        Total Pembayaran
                      </h1>
                      <h1 className="font-semibold text-sm">{`$${countTotalPrice(
                        cartData.products
                      )}`}</h1>
                    </div>
                    <button className="bg-main rounded-full mt-4 w-full">
                      <h2 className="font-semibold text-sm py-4 text-white">
                        Buat Pesanan
                      </h2>
                    </button>
                  </div>
                </>
              )}
            </div>
          </>
        )}
      </div>
    </>
  );
}
