/* eslint-disable @next/next/no-img-element */
import { useRouter } from "next/router";
import { useState, useEffect } from "react";

import Loading from "components/Loading";

import { Splide, SplideSlide } from "@splidejs/react-splide";
import Skeleton from "react-loading-skeleton";
import "@splidejs/react-splide/css";
import "react-loading-skeleton/dist/skeleton.css";
import BackButton from "components/Button/BackButton";
import CategoryLabel from "components/Category/CategoryLabel";
import ProductDescription from "components/ProductCard/ProductDescription";
import MainContainer from "components/Container/MainContainer";
import getProductById from "data/getProductById";
import addProduct from "data/addProduct";

export default function ProductDetail() {
  const [productData, setProductData] = useState({
    images: [],
  });
  const [isLoading, setIsLoading] = useState(false);
  const [imageIsLoading, setImageIsLoading] = useState(true);

  const router = useRouter();
  const { id } = router.query;

  const addProductHandler = async (product) => {
    addProduct(product).then((data) => {
      localStorage.setItem("cart", JSON.stringify(data));
      router.push("/cart");
    });
  };

  useEffect(() => {
    setIsLoading(true);
    getProductById(id).then((product) => {
      setProductData(product);
      setIsLoading(false);
    });
  }, []);

  return (
    <>
      <div className="flex flex-col min-h-screen max-h-full bg-white">
        {isLoading ? (
          <div className="mx-4 my-4 min-h-screen flex flex-col justify-center">
            <Loading />
          </div>
        ) : (
          <MainContainer>
            <BackButton />
            {imageIsLoading ? <Skeleton height={250} /> : null}
            <Splide
              aria-label="Products"
              className="mt-3"
              options={{
                rewind: true,
                gap: "1rem",
              }}
            >
              {productData.images.map((image) => (
                <SplideSlide key={image} className="my-auto">
                  <img
                    alt="alt"
                    src={image}
                    className="w-full rounded-lg"
                    onLoad={() => {
                      setImageIsLoading(false);
                    }}
                  />
                </SplideSlide>
              ))}
            </Splide>
            <CategoryLabel category={productData.category} />
            <ProductDescription
              title={productData.title}
              price={productData.price}
              description={productData.description}
            />
            <button
              onClick={() => {
                addProductHandler(productData);
              }}
              className="bg-main rounded-full mt-5 w-full"
            >
              <h2 className="font-semibold text-sm py-4 text-white">
                Tambah Pesanan
              </h2>
            </button>
          </MainContainer>
        )}
      </div>
    </>
  );
}
