/* eslint-disable @next/next/no-img-element */
import { useState, useEffect } from "react";
import Image from "next/image";
import Link from "next/link";

import Loading from "components/Loading";
import MainContainer from "components/Container/MainContainer";
import LogoutButton from "components/Button/LogoutButton";
import CategoryOption from "components/Category/CategoryOption";
import getProductCategories from "data/getProductCategories";

export default function ProductCategory() {
  const [categories, setCategories] = useState([""]);
  const [isLoading, setIsLoading] = useState(false);
  const [countCart, setCountCart] = useState(0);

  useEffect(() => {
    setIsLoading(true);
    getProductCategories().then((data) => {
      setCategories(data);
      setIsLoading(false);
    });

    // Get cart items length
    if (localStorage.getItem("cart")) {
      const items = JSON.parse(localStorage.getItem("cart"));
      const count = items.products.length;
      setCountCart(count);
    }
  }, []);

  return (
    <>
      <div className="flex flex-col min-h-screen max-h-full bg-white">
        {isLoading ? (
          <div className="mx-4 my-4 min-h-screen flex flex-col justify-center">
            <Loading />
          </div>
        ) : (
          <>
            <MainContainer>
              <div className="flex justify-between items-center">
                <h1 className="text-black my-auto text-base font-semibold mb-5">
                  Kategori Produk
                </h1>

                <Link href="/cart" passHref={true}>
                  <div className="relative h-10 w-10 flex items-center justify-center rounded-full border border-gray-300 cursor-pointer">
                    <Image
                      src="/icon/icon-cart.svg"
                      height={24}
                      width={24}
                      alt="icon cart"
                    />
                    <p className="absolute top-0 right-0 flex justify-center items-center text-[8px] font-bold w-3 h-3 my-auto rounded-full bg-green-600 text-white">
                      {countCart}
                    </p>
                  </div>
                </Link>
              </div>
              <div className="grid grid-cols-4 mt-2 justify-between gap-5">
                {categories.map((category, id) => (
                  <CategoryOption key={id} id={id} category={category} />
                ))}
              </div>
              <LogoutButton />
            </MainContainer>
          </>
        )}
      </div>
    </>
  );
}
